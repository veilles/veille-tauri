import './App.css';

import { createSignal } from 'solid-js';

import { invoke } from '@tauri-apps/api/tauri';
import { isPermissionGranted, Options, requestPermission, sendNotification } from '@tauri-apps/api/notification';

import logo from './assets/logo.svg';
import icon from './assets/32x32.png';

function App() {
  const [greetMsg, setGreetMsg] = createSignal("");
  const [name, setName] = createSignal("");

  async function greet() {
    setGreetMsg(await invoke("greet", { name: name() }));
  }

  async function notif() {
    let perm: boolean = await isPermissionGranted();

    if (perm === false) {
      const permission = await requestPermission();
      perm = permission === 'granted';
    }
    console.log(perm);
    
    const notif: Options = {
      title: "Tauri Tauri",
      body: "Slt les amis",
      icon: icon
    }
    if (perm === true) sendNotification(notif);
  }

  notif();

  return (
    <div class="container">
      <h1>SLT</h1>

      <div class="row">
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" class="logo vite" alt="Vite logo" />
        </a>
        <a href="https://tauri.app" target="_blank">
          <img src="/tauri.svg" class="logo tauri" alt="Tauri logo" />
        </a>
        <a href="https://solidjs.com" target="_blank">
          <img src={logo} class="logo solid" alt="Solid logo" />
        </a>
      </div>

      <p>Click on the Tauri, Vite, and Solid logos to learn more.</p>

      <div class="row">
        <div>
          <input
            id="greet-input"
            onChange={(e) => setName(e.currentTarget.value)}
            placeholder="Enter a name..."
          />
          <button type="button" onClick={() => greet()}>
            Greet
          </button>
        </div>
      </div>

      <p>{greetMsg()}</p>
    </div>
  );
}

export default App;
